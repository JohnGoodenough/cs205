﻿namespace _2_Adventure{
    public struct Armour{
        public Armour (string _name, int _hp, int _defence, int _spirit, int _evasion) {
            name = _name;
            hp = _hp;
            defence = _defence;
            spirit = _spirit;
            evasion = _evasion;
        }

        public string name;
        public int hp;
        public int defence;
        public int spirit;
        public int evasion;
    }
}
