﻿using System;

namespace _2_Adventure {
    class Growth {
        public Growth(Character _character) {
            SetGrowthRates(_character);
        }

        public float hp;
        public float mp;
        public float strength;
        public float defense;
        public float intelligence;
        public float spirit;
        public float accuracy;
        public float evasion;
        public float luck;

        public void Log() {
            Console.WriteLine("hp          : " + hp);
            Console.WriteLine("mp          : " + mp);
            Console.WriteLine("strength    : " + strength);
            Console.WriteLine("defense     : " + defense);
            Console.WriteLine("intelligence: " + intelligence);
            Console.WriteLine("spirit      : " + spirit);
            Console.WriteLine("accuracy    : " + accuracy);
            Console.WriteLine("evasion     : " + evasion);
            Console.WriteLine("luck        : " + luck);
        }

        private void SetGrowthRates(Character _character) {
            int[] values = { _character.strength, _character.defense, _character.intelligence, _character.spirit, _character.accuracy, _character.evasion, _character.luck };
            int min = values[0];
            int max = values[0];

            for (int i = 0; i < values.Length - 1; i++) {
                min = Math.Min(min, values[i + 1]);

                //Console.WriteLine(values[i]);
                if (i == values.Length - 2) {
                    //Console.WriteLine(values[i + 1]);
                }

                max = Math.Max(max, values[i + 1]);
            }

            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine("Minimum: " + min);
            //Console.WriteLine("Maximum: " + max);

            hp += 1.12f;
            mp += 1.2f;
            strength = GetGrowthValue(ref _character.strength);
            defense = GetGrowthValue(ref _character.defense);
            intelligence = GetGrowthValue(ref _character.intelligence);
            spirit = GetGrowthValue(ref _character.spirit);
            accuracy = GetGrowthValue(ref _character.accuracy);
            evasion = GetGrowthValue(ref _character.evasion);
            luck = 1.1f;

            float GetGrowthValue(ref int _property) {
                return 1 + (float)(_property - min) / (max - min);
            }
        }
    }
}
