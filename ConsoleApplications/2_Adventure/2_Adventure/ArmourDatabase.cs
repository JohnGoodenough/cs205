﻿using System.Collections.Generic;

namespace _2_Adventure{
    class ArmourDatabase {
        public static Dictionary<string, Armour> Armor = new Dictionary<string, Armour>() {
            { "Heavy Cloth"  , new Armour("Heavy Cloth"  , 8,  6,  4,  10) },
            { "Leather Plate", new Armour("Leather Plate", 20, 9,  9,  8) },
            { "Kevlar Vest"  , new Armour("Kevlar Vest"  , 28, 14, 15, 4) }
};
    }
}
