﻿namespace _2_Adventure {
    public struct Armor {
        public Armor(string _name, int _hp, int _defense, int _spirit, int _evasion) {
            name = _name;
            hp = _hp;
            defense = _defense;
            spirit = _spirit;
            evasion = _evasion;
        }

        public string name;
        public int hp;
        public int defense;
        public int spirit;
        public int evasion;
    }
}
