﻿using System;

namespace _2_Adventure {
    public struct ColoredText {
        public ColoredText(string _text, ConsoleColor _consoleColor = ConsoleColor.Gray) {
            text = _text;
            consoleColor = _consoleColor;
        }

        public string text;
        public ConsoleColor consoleColor;
    }
}
