﻿using System;
using System.Collections.Generic;

namespace _1_Quiz{
    class Program{
        static void Main() {
            new Question("Shall We Play?"
                , new Dictionary<string, Action> {
                {  "yes", () => {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("\nWoo! Let's play!! Press ASDFGHJK!!!!");

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Press Q to quit \n");
                        Console.ResetColor();

                        new Piano();
                    }
                }, {"no", () => {
                    Console.WriteLine("AAAARRRGGGHH!");

                    new Question(
                        "You sure about that?",
                        new Dictionary<string, Action>
                        {
                            { "mm-hmmm", () =>
                                {
                                   Console.WriteLine("Fine :'(");
                                }
                            }
                        }
                        );
                    }
                    }
                });

            Console.Beep(200, 100);
            Console.Beep(160, 200);
            Console.WriteLine("\nThanks for playing! Press any key to close.");

            Console.ReadKey(true);
        }
    }
}
