﻿using UnityEngine;
using TMPro;

public class Pitcher : MonoBehaviour {
    public Rigidbody pitchedBall;
    public float timeLeft = 5.0f;
    public TextMeshProUGUI pitcherText;

    private float pitchAngle;
    private float curve;
    private int speed;
    public bool timerFinshed;


    private void Update() {
        PitchBall();
    }

    private void PitchTimer() {
        timerFinshed = false;

        timeLeft -= Time.deltaTime;
        pitcherText.text = (timeLeft).ToString("0");
        if (timeLeft < 0) {
            timerFinshed = true;
            timeLeft = 5.0f;
        }
    }

    private void PitchBall() {
        PitchTimer();
        if (timerFinshed) {

            curve = Random.Range(-35f, 35f);
            speed = Random.Range(24, 41);
            pitchAngle = Random.Range(-3, -5);

            Debug.Log(speed);
            Debug.Log(pitchAngle);
            Debug.Log(curve);

            transform.Rotate(pitchAngle, 0, 0);

            Rigidbody rb = Instantiate(pitchedBall, transform.position, transform.rotation);
            rb.velocity = transform.TransformDirection(Vector3.forward * speed);
            rb.AddForce(curve, 0, 0);

            transform.Rotate(-pitchAngle, 0, 0);
            timerFinshed = false;
        }
    }
}

