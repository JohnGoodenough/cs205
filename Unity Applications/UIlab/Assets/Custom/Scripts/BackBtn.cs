﻿using UnityEngine;
using UnityEngine.UI;

public class BackBtn : MonoBehaviour {
    public Button backBtn;
    public RectTransform pause;
    public RectTransform optionsPanel;

    void Update() {
        Button btn = backBtn.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick() {
        pause.gameObject.SetActive(true);
        optionsPanel.gameObject.SetActive(false);
    }
}
