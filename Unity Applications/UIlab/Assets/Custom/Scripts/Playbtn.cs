﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Playbtn : MonoBehaviour {
    public Button startBtn;


    void Start() {
        Button btn = startBtn.GetComponent<Button>();
        btn.onClick.AddListener(() => {
            TaskOnClick();
        });
    }

    private void TaskOnClick() {
        Invoke("LoadGame", 0f);
    }

    private void LoadGame() {
        SceneManager.LoadScene("Game");
    }
}
