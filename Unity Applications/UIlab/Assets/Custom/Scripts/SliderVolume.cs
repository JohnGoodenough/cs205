﻿using UnityEngine;
using UnityEngine.UI;

public class SliderVolume : MonoBehaviour {
    public Slider slider;
    public AudioSource audio;

    private void Start() {
        slider.onValueChanged.AddListener((e) => { audio.volume = e;} );
    }

}
