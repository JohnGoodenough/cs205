﻿using UnityEngine;
using UnityEngine.UI;

public class QuitGame : MonoBehaviour {
    public Button startBtn;

    void Start() {
        Button btn = startBtn.GetComponent<Button>();
        btn.onClick.AddListener(() =>
        {
            TaskOnClick();
        });
    }

    private void TaskOnClick() {
        Application.Quit();
    }

}
