﻿using UnityEngine;
using UnityEngine.UI;

public class ResumeBtn : MonoBehaviour {
    public RectTransform pause;
    public Button resume;
    
    void Start() {
        Button btn = resume.GetComponent<Button>();
        btn.onClick.AddListener(() => {
            TaskOnClick();
        });
    }

    private void TaskOnClick() {
        pause.gameObject.SetActive(false);
    }
}
