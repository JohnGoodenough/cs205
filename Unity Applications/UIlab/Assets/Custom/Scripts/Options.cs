﻿using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour {
    public Button optionsBtn;
    public RectTransform pause;
    public RectTransform optionsPanel;

    void Update() {
        Button btn = optionsBtn.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick() {
        pause.gameObject.SetActive(false);
        optionsPanel.gameObject.SetActive(true);
    }

}
