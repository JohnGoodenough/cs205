﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuitBtn : MonoBehaviour {
    public Button quitBtn;

    void Start() {
        Button btn = quitBtn.GetComponent<Button>();
        btn.onClick.AddListener(() => {
            TaskOnClick();
        });
    }

    private void TaskOnClick() {
        Invoke("LoadGame", 0f);
    }

    private void LoadGame() {
        SceneManager.LoadScene("StartScreen");
    }
}
