﻿using UnityEngine;
using UnityEngine.UI;

public class Mute : MonoBehaviour {
    public Toggle mute;
    public AudioSource audio;

    void Start() {
        mute.onValueChanged.AddListener((e) => { audio.mute = e; });
    }

}
