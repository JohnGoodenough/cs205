﻿using UnityEngine;

public class Sensor : MonoBehaviour {

    private Animator anim;

    private void Awake() {
        anim = transform.GetChild(0).GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player" && Player.Instance.hasKey) {
            anim.SetBool("isOpen", true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Player" ) {
            anim.SetBool("isOpen", false);
        }
    }
}
