﻿//using UnityEngine;

//public class Pitcher : MonoBehaviour {
//    public Rigidbody pitchedball;
//    public float speed = 5;
//    public ViveInputs input;

//    private Rigidbody spawnedBall;

//    private void Update() {

//        if (input.vrTriggerButtonDown) {
//            if (spawnedBall) {
//                Destroy(spawnedBall.gameObject);
//            }

//            spawnedBall = Instantiate(pitchedball, transform.position, transform.rotation);
//            spawnedBall.velocity = transform.forward * speed;

//        }
//    }
//}

using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Pitcher : MonoBehaviour {
    public Rigidbody pitchedBall;
    public float timeLeft = 5.0f;
    public bool timerFinshed;
    public ViveInputs input;

    public TextMeshProUGUI pitcherText;
    public TextMeshProUGUI ballsRemaining;

    //    private Rigidbody spawnedBall;
    private float pitchAngle;
    private float curve;
    private int speed;
    private int counter = 15;
    private Rigidbody rb;


    private void Update() {
        PitchBall();
        ballsRemaining.text = "Balls Remaining: " + counter;
        if (counter <= 0) {
            // end game.
            ballsRemaining.text = "GAMEOVER";
            Invoke("EndGame", 3.0f);
        }
    }

    private void PitchTimer() {
        timerFinshed = false;

        timeLeft -= Time.deltaTime;
        pitcherText.text = (timeLeft).ToString("0");
        if (timeLeft < 0) {
            timerFinshed = true;
            timeLeft = 5.0f;
        }
    }

    private void PitchBall() {
        PitchTimer();
        if (timerFinshed/*input.vrTriggerButtonDown*/) {

            curve = Random.Range(-30f, 30f);
            speed = Random.Range(32, 43);
            pitchAngle = Random.Range(-5, -7);

            //Debug.Log(speed);
            //Debug.Log(pitchAngle);
            //Debug.Log(curve);

            transform.Rotate(pitchAngle, 0, 0);

            rb = Instantiate(pitchedBall, transform.position, transform.rotation);
            rb.velocity = transform.TransformDirection(Vector3.forward * speed);
            rb.AddForce(curve, 0, 0);

            transform.Rotate(-pitchAngle, 0, 0);
            timerFinshed = false;
            counter--;
        }
    }

    public void EndGame() {
        SceneManager.LoadScene("StartScreen");
    }
}