﻿using UnityEngine;
using Valve.VR;

public class ViveInputs : MonoBehaviour {
    public SteamVR_ActionSet actionset;

    protected SteamVR_Behaviour_Pose controller;
    protected SteamVR_Input_Sources handtype;

    #region Trigger
    public bool vrTriggerButton;
    public bool vrTriggerButtonDown;
    public bool vrTriggerButtonUp;
    #endregion

    #region Menu
    protected bool vrApplicationMenuPress;
    #endregion

    private SteamVR_Input_ActionSet_john actions;

    protected virtual void Awake() {
        actions = SteamVR_Actions.john;
        controller = GetComponent<SteamVR_Behaviour_Pose>();
        handtype = controller.inputSource;

    }
    protected virtual void Start() {
        actionset.Activate(handtype, 0, false);
    }

    protected virtual void Update() {
        SetInputs();
        //DebugInputs();
    }

    protected void SetInputs() {
        vrTriggerButton = actions.buttonTrigger.GetState(handtype);
        vrTriggerButtonDown = actions.buttonTrigger.GetStateDown(handtype);
        vrTriggerButtonUp = actions.buttonTrigger.GetStateUp(handtype);
        vrApplicationMenuPress = actions.buttonMenu.GetState(handtype);
    }

    protected void DebugInputs() {
        if (vrApplicationMenuPress) {
            Debug.Log("applicationmenu");
        }
        if (vrTriggerButton) {
            Debug.Log("TriggerButton");
        }
    }
}