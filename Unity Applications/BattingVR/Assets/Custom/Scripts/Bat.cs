﻿using UnityEngine;
using Valve.VR;

public class Bat : MonoBehaviour
{
    public float hitPower = 20;
    public float threshold = 2;
    public SteamVR_Action_Vibration hapticAction;
    public SteamVR_Input_Sources handType;
    public GameObject batExplosion;

    private Vector3 controllerVelocity;


    private void OnCollisionEnter(Collision collision) {
        HitBall(collision);

        hapticAction.Execute(0, 0.25f, 150, 1, handType);
    }

    public void UpdatePhysics(Vector3 velocity) {
        controllerVelocity = velocity;
    }

    private void HitBall(Collision collision) {
        if (collision.gameObject.tag == "Ball") {
            bool hasMetThreshold = gameObject.GetComponent<Rigidbody>().velocity.magnitude > threshold;

                Debug.Log("con velocity" + gameObject.GetComponent<Rigidbody>().velocity);
                Debug.Log("Magnitude " + gameObject.GetComponent<Rigidbody>().velocity.magnitude);

            if (hasMetThreshold) {

                GameObject explosion = Instantiate(batExplosion, transform.position, Quaternion.identity);
                explosion.GetComponent<ParticleSystem>().Play();

                //collision.gameObject.GetComponent<Rigidbody>().velocity = controllerVelocity * hitPower;
                Vector3 v = collision.gameObject.GetComponent<Rigidbody>().velocity;
                collision.gameObject.GetComponent<Rigidbody>().velocity = -v * hitPower;
            }

            //GetComponent<Renderer>().material.color = hasMetThreshold ? Color.blue : Color.red;
        }
    }

}
