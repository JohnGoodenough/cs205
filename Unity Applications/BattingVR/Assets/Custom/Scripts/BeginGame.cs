﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BeginGame : MonoBehaviour {

    private void OnCollisionEnter(Collision collision) {
        Debug.Log("works");
        if (collision.gameObject.tag == "bat") {
            Invoke("LoadScene", 2.0f);

        }
    }

    public void LoadScene() {
        SceneManager.LoadScene("GameScene1");
    }
}
