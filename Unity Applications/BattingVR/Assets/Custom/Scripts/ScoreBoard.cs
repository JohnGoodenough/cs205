﻿using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour {
    public static ScoreBoard instance;
    private float bestHit;

    public TextMeshProUGUI txtDistance;
    public TextMeshProUGUI txtBestDistance;

    private void Start() {
        instance = this;

    }

    public void TestScoreBoard(float ballDistance) {
        txtDistance.text = "Distance: " + ballDistance.ToString("f2");

        if (ballDistance > bestHit) {
            bestHit = ballDistance;
        }

        txtBestDistance.text = "Best Hit: " + bestHit.ToString("f2");
    }
}
