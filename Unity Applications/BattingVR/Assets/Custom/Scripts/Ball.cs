﻿using UnityEngine;



public class Ball : MonoBehaviour {


    private float ballDistance;
    private Vector3 homeBase;
    private Vector3 ballPosition;
    private Rigidbody rb;
    private bool isHit;

    private void Awake() {
        rb = GetComponent<Rigidbody>();

    }

    private void Start() {
        homeBase = Camera.current.transform.position;
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "bat") {
            isHit = true;
        }
        
    }

    private void Update() {
        ballPosition = transform.position;
        ballDistance = Vector3.Distance(ballPosition, homeBase);
       
        if (isHit && rb.velocity.magnitude > 0.01f) {
            ScoreBoard.instance.TestScoreBoard(ballDistance);
        }

        if (rb.position.y < 0) {
            Destroy(rb.gameObject);
        }
    }


}
