//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public class SteamVR_Input_ActionSet_john : Valve.VR.SteamVR_ActionSet
    {
        
        public virtual SteamVR_Action_Boolean buttonTrigger
        {
            get
            {
                return SteamVR_Actions.john_buttonTrigger;
            }
        }
        
        public virtual SteamVR_Action_Boolean buttonMenu
        {
            get
            {
                return SteamVR_Actions.john_buttonMenu;
            }
        }
        
        public virtual SteamVR_Action_Vibration haptic
        {
            get
            {
                return SteamVR_Actions.john_haptic;
            }
        }
    }
}
