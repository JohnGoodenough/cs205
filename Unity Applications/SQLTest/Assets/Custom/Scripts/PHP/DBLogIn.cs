﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public static class DBLogIn {
    public const string URI_LOGIN = "http://localhost/_unity_projects/unity_test/signUp.php";

    private static IEnumerator LogIn(string _email, string _password) {
        WWWForm form = new WWWForm();
        form.AddField("email", _email);
        form.AddField("password", _password);

        UnityWebRequest webRequest = UnityWebRequest.Post(URI_LOGIN, form);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError) {
            Debug.Log("Error: " + webRequest.error);
        }
        else {
            string contents = webRequest.downloadHandler.text;

            if (contents.Contains("Error")) {
                Debug.Log(contents);
                yield break;
            }

            string[] properties = contents.Split('|');

            UserData.CurrentUserData = new UserData(
                int.Parse(GetDataValue(properties[0], "id"))
            ,   GetDataValue(properties[1], "email")
            ,   GetDataValue(properties[2], "password")
            );

            Debug.Log(UserData.CurrentUserData.email + " | " + UserData.CurrentUserData.password);

            Events.LogIn();
        }
    }

    private static string GetDataValue(string _data, string _propertyString) {
        _propertyString += ":";
        string value = _data.Substring(_data.IndexOf(_propertyString) + _propertyString.Length);
        value = value.Replace("<br>", "");

        if (value.Contains("|")) {
            value = value.Remove(value.IndexOf("|"));
        }

        return value;
    }
}