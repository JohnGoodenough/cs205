﻿using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

public static class DBSignUp {
    public const string URI_SIGN_UP = "http://localhost/_unity_projects/unity_test/signUp.php";

    public static void SignUp(string _email, string _password) {
        if (string.IsNullOrEmpty(_email)|| IsValidEmailAddress(_email) || string.IsNullOrEmpty(_password)) {
            Debug.Log("Invalid details");
            return;
        }

        Debug.Log("You Have Signed Up");


    }
    

    private static bool IsValidEmailAddress(string s) {
        var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        return regex.IsMatch(s);
    }

    private static IEnumerator CreateUser(string _email, string _password) {
        WWWForm form = new WWWForm();
        form.AddField("email", _email);
        form.AddField("password", _password);

        UnityWebRequest webRequest = UnityWebRequest.Post(URI_SIGN_UP, form);
        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError) {
            Debug.Log("Error: " + webRequest.error);
        } else {
            Debug.Log(webRequest.downloadHandler.text);
        }

        CoroutinePawn.Instance
    }
}
