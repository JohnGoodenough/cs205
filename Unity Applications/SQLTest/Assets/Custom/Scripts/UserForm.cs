﻿using UnityEngine;
using UnityEngine.UI;

public class UserForm : MonoBehaviour {
    public InputField inputEmail;
    public InputField inputPassword;
    public Button btnLogin;

    private GameObject panel;

    private void OnEnable() {
        Events.OnLogIn += OnLogIn;
        Events.OnLogOut += OnLogOut;
    }

    private void OnDisable() {
        Events.OnLogIn -= OnLogIn;
        Events.OnLogOut -= OnLogOut;
    }

    private void Awake() {
        btnLogin.onClick.AddListener(() => { DBSignUp.SignUp(inputEmail.text, inputPassword.text); });

        panel = transform.GetChild(0).gameObject;

        OnLogOut();
    }

    private void OnLogIn() {
        panel.SetActive(false);
        inputEmail.text = "";
        inputPassword.text = "";
    }

    private void OnLogOut() {
        panel.SetActive(true);
    }
}
