﻿using UnityEngine;
using Photon.Pun;
using System;

public class ControllerThirdPersonOrbitPUN : ControllerThirdPersonOrbit, IPunObservable {
    public int hp = 10;
    public string prfBulletPath = "prefabs/";
    public float bulletSpawnDistance = 1.0f;
    public CharacterWorldGUI gui;

    private int maxHp;
    private Vector3 latestPosition;

    protected override void Awake() {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;

        if (photonView.IsMine) {
            FindObjectOfType<CameraOrbit>()?.SetTarget(transform);

            PhotonNetwork.SendRate = 20;
            PhotonNetwork.SerializationRate = 20;
        }

        gameObject.tag = photonView.IsMine ? "Player" : "Enemy";
        gui.SetText(photonView.Owner.NickName);
        maxHp = hp;
    }

    protected override void Update() {
        if (photonView.IsMine) { 
            base.Update();

            if (Input.GetMouseButtonDown(0)) {
                photonView.RPC("Example", RpcTarget.All, true);
                Shoot();
            } else if (Input.GetMouseButtonUp(0)) {
                photonView.RPC("Example", RpcTarget.All, false);
            }
        } else {
            smoothMove();

        }
    }

    private void smoothMove() {
        transform.position = Vector3.Lerp(transform.position, latestPosition, Time.deltaTime * 10);
    }

    protected override void FixedUpdate() {
        if (photonView.IsMine) {
            base.FixedUpdate();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(transform.position);
        } else if (stream.IsReading) {
            latestPosition = (Vector3)stream.ReceiveNext();
        }
    }

    [PunRPC]
    private void Example(bool _isDown, PhotonMessageInfo _info) {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();

        foreach (Renderer r in renderers) {
            r.material.color = _isDown ? Color.green : Color.blue;
        }
    }

    [PunRPC]
    public void TakeDamage(int _damage) {
        hp -= _damage;
        gui.SetHealthGauge((float)hp / maxHp);

        if(photonView.IsMine && hp <= 0) {
            PhotonNetwork.Destroy(photonView);
        }
    }

    public void Shoot() {
        Vector3 spawnPoint = transform.position + (transform.forward * bulletSpawnDistance) + (Vector3.up * 1.5f);
        Bullet newBullet = PhotonNetwork.Instantiate(prfBulletPath, spawnPoint, Quaternion.identity).GetComponent<Bullet>();
        newBullet.photonView.RPC("SetVelocity", RpcTarget.AllBuffered, transform.forward);
    }
}
