﻿using Photon.Pun;
using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviourPun {
    public int damage = 1;
    public float lifetime = 10;
    public float speed = 10;

    private Rigidbody rb;

    private void Awake() {
        rb = GetComponent<Rigidbody>();

        StartCoroutine(Lifetime());
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Enemy" && photonView.IsMine) {
            collision.gameObject.GetPhotonView().RPC("TakeDamage", RpcTarget.All, damage);

            PhotonNetwork.Destroy(photonView);
        }

    }

    private IEnumerator Lifetime() {
        yield return new WaitForSeconds(lifetime);


        if (PhotonNetwork.IsMasterClient) {
            PhotonNetwork.Destroy(photonView);
        }
    }

    [PunRPC]
    public void SetVelocity(Vector3 _direction) {
        rb.velocity = _direction * speed;
    }
}
