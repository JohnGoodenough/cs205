﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTest : MonoBehaviour
{
    private Material mat;

    private void OnCollisionEnter(Collision collision) {
        mat.color = Color.red;
        Destroy(collision.gameObject);
    }

    private void OnCollisionExit(Collision collision) {
        mat.color = Color.yellow;
    }
    private void Start() {
        mat = GetComponent<Renderer>().material;
    }
}
