﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTest : MonoBehaviour
{
    private Material mat;

    private void Start() {
        mat = GetComponent<Renderer>().material;
    }

    private void OnTriggerEnter(Collider other) {
        mat.color = Color.red;
    }

    private void OnTriggerExit(Collider other) {
        mat.color = Color.yellow;
    }
}
