﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableRigidbody : MonoBehaviour {

    public float force = 5;
    private Camera mainCamera;
    private Rigidbody selectedRigidbody;
    private Vector3 originalMousePosition;
    private Vector3 originalObjectPosition;
    private float selectionDistance;

    private void Awake() {
        mainCamera = Camera.main;
    }

    private void Update() {
        if (!mainCamera) {
            return;
        }

        if (Input.GetMouseButtonDown(0)) {
            selectedRigidbody = GetRigidbodyFromMouseClick();
        }

        if (Input.GetMouseButtonUp(0)) {
            selectedRigidbody = null;
        }

    }

    private Rigidbody GetRigidbodyFromMouseClick() {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit)) {
            Rigidbody selected = hit.collider.gameObject.GetComponent<Rigidbody>();

            if (selected) {
                selectionDistance = Vector3.Distance(ray.origin, hit.point);
                originalMousePosition = GetMouseWorldPosition();
                originalMousePosition = hit.transform.position;

                return selected;
            }
        }

        return null;
    }

    private void FixedUpdate() {
        if (selectedRigidbody) {
            Vector3 mousePositionOffset = GetMouseWorldPosition() - originalMousePosition;
            selectedRigidbody.velocity = ((originalObjectPosition + mousePositionOffset) - selectedRigidbody.transform.position) * force;
        }
    }

    private Vector3 GetMouseWorldPosition() {
        Vector2 mousePosition = Input.mousePosition;
        return mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.y, selectionDistance));
    }
}
