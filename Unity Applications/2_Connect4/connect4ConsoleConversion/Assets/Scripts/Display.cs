﻿using System;
using UnityEngine;

internal class Display {
    public Display() {
        //Console.WindowHeight = Game.BoardSize.y + 10;
        //Console.WindowWidth = Game.BoardSize.x + 5;
    }

    public void Render() {
        Console.SetCursorPosition(0, 0);

        string output = "";

        for (int y = Game.BoardSize.y - 1; y >= 0; y--) {
            output += "||\t";

            for (int x = 0; x < Game.BoardSize.x; x++) {
                output += GetObjectColor (Game.Instance.board[x, y]) + Game.Instance.board[x, y] + "</color>\t";

            }

            output += "||\n";
        }

        RenderGuide(ref output);

        Debug.Log(output);

    }

    private static void RenderGuide(ref string output) {
        output +=("|| |t");
        for (int x = 0; x < Game.BoardSize.x; x++) {
           output += (x + 1) + "|t ";
        }
        output += "||";
    }

    private string GetObjectColor(char _c) {
        string value = "";

        switch (_c) {
            case 'O':
                value = "red";
                break;
            case 'X':
                value = "yellow";
                break;
            default:
                value = "black";
                break;
        }

        return "<color=" + value + ">";
    }

    void RenderVerticalBoundary() {
        Console.ForegroundColor = ConsoleColor.Cyan;
        for (int i = 0; i <= (Game.BoardSize.x * 2) + 4; i++) {
            Console.Write('=');
        }
    }


}
