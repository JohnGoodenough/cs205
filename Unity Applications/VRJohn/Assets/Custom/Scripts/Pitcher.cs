﻿using UnityEngine;

public class Pitcher : MonoBehaviour {
    public Rigidbody pitchedball;
    public float speed = 5;
    public ViveInputs input;

    private void Update() {

        if (input.vrTriggerButtonDown) {
            Rigidbody p = Instantiate(pitchedball, transform.position, transform.rotation);
            p.velocity = transform.forward * speed;
        }
    }
}
